// ex1
function ex1TimMin(){
    var thongBao = document.getElementById("ex1-result");
    var tong = 0;

    for(var i=1; tong<10000;i++){
        tong += i;
    };

    i--;

    thongBao.innerHTML = `Số nguyên dương nhỏ nhất: ${i}`;
};

// ex2
function ex2TinhTong(){
    var thongBao = document.getElementById("ex2-result");
    var x = document.getElementById("ex2-x").value *1;
    var n = document.getElementById("ex2-n").value *1;
    var tong = 0;
    var replace = x;

    for(var i=1; i<=n; i++){
        tong += x;
        x = x*replace;
    };

    thongBao.innerHTML = `Tổng: ${tong}`;
};

// ex3
function ex3TinhGiaiThua(){
    var thongBao = document.getElementById("ex3-result");
    var n = document.getElementById("ex3-n").value *1;
    var giaiThua = 1;
    
    for(var i=1; i<=n; i++){
        giaiThua = giaiThua * i;
    };
    
    if (n==0 || n == ""){
        thongBao.innerHTML = `Giai thừa của 0 = 0`;
    } else{
        thongBao.innerHTML = `Giai thừa của ${n} = ${giaiThua}`;
    };
};

// ex4
function ex4TaoDiv(){
    var thongBao = document.getElementById("ex4-result");

    for(var i =1; i<=10; i++){
        if(i%2 !=0){
            le = `<p style='background: blue; padding-left: 8px; margin: 0'>Div lẻ ${i}</p>`;
            thongBao.insertAdjacentHTML("beforeend", le);
        } else{
            chan = `<p style='background: red; padding-left: 8px; margin: 0'>Div chẵn ${i}</p>`;
            thongBao.insertAdjacentHTML("beforeend", chan);
        }
    };
};